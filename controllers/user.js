/*
 *
 * User controller
 *
 * * * * * * * * * * * * * * * * * *  */

// Load the model
const UserModel = require('../models/User');

const UserCtrl = new Object() ;

// Create endpoint /api/users for POST
UserCtrl.postUser = function(req, res) {
  //console.log("Post user");
  var user = new UserModel({
    username: req.body.login,
    email: req.body.email,
    password: req.body.password
  });

  user.save(function(err) {
    if (err) res.send(err);
    else res.json({ message: 'New User added' });
  });
};

// Create endpoint /api/users for GET
UserCtrl.getUsers = function(req, res) {
  UserModel.find(function(err, users) {
    if (err) res.send(err);
    else res.json(users);
  });
};
module.exports = UserCtrl ;
