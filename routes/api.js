module.exports = function(passport) {
  var express = require('express');
  var router = express.Router();
  const mongoose = require('mongoose');


  var launchPointsCtrl = require('../controllers/launchPoint');
  var userCtrl = require('../controllers/user');

  var router = express.Router();
  router.route('/')
    .get(launchPointsCtrl.welcome) ;
  router.route('/signup')
    .post(userCtrl.postUser) ;
  router.route('/points')
    .post(launchPointsCtrl.postPoint)
    .get(launchPointsCtrl.getPoints) ;
  router.route('/points/import')
    .get(launchPointsCtrl.import) ;
  router.route('/points/:id')
    .get(launchPointsCtrl.getPoint)
    .put(launchPointsCtrl.putPoint)
    .delete(launchPointsCtrl.deletePoint);
  router.route('/points/search/:search')
    .get(launchPointsCtrl.searchPoint) ;
  router.route('/points/bounds/:topleft/:bottomright')
    .get(launchPointsCtrl.listPointsInBounds) ;
  return router
}
// module.exports = router;
