const URL_PROD = 'mongodb://admin:pass@apollo.modulusmongo.net:27017/Guja3paw' ;
const URL_DEV = 'mongodb://localhost:27017/MALK' ;

var config = {}
if(process.env.NODE_ENV == "production"){
  config.url = URL_PROD ;
}else{
  config.url = URL_DEV ;
}
module.exports = config ;
