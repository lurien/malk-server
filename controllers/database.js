// db requires
var mongoose = require('mongoose');

exports.connect = function (urlDb){
  mongoose.connect(urlDb) ;
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open',  console.error.bind(console, 'Mongo Db Connected'));
  return db ;
}
