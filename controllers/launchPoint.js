/*
 *
 * LaunchPoint controller
 *
 * * * * * * * * * * * * * * * * * *  */

// Load the model
const LaunchPointModel = require('../models/LaunchPoint');

const LaunchPointCtrl = new Object() ;
LaunchPointCtrl.welcome = function(req,res){
  res.send('Welcome to the MALK api');
} ;

/**
 * List all points
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.getPoints = function(req,res){
  LaunchPointModel.find({},function(err,points){
    if (err) return res.send(err) ;
    else return res.json(points) ;
  }) ;
} ;

/**
 * Post a new point
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.postPoint = function(req,res){

  var data = req.body ;
  var p = new LaunchPointModel(data) ;
  p.save(function (err, r) {
    if (err) return res.send(err) ;
    else return res.send(r) ;
  }) ;
} ;

/**
 * Get a point by id
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.getPoint = function(req,res){
  var id = req.params.id;
  LaunchPointModel.find({_id:id}, function(err,points){
    if (err) return res.json(err) ;
    else return res.json(points) ;
  }) ;
} ;

/**
 * Update a point by id
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.putPoint = function(req,res){
  var id = req.params.id;
  var data = req.body;
  LaunchPointModel.update({_id:id},data,{},function (err, p) {

    if (err) return res.send(err) ;
    else return res.json(p) ;
  }) ;
} ;

/**
 * Delete a point by id
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.deletePoint = function(req,res){
  // @TODO
} ;


/**
 * Search a point by name
 *
 * @param req, res
 * @api private
 */
LaunchPointCtrl.searchPoint = function(req,res){
  var search = req.params.search ;
  var regex = new RegExp(search, 'i');
  var c = {'properties.name': regex} ;
  LaunchPointModel.find(c).limit(20).exec(function(err,points){
    if (err) return res.send(err) ;
    else return  res.json(points) ;
  }) ;
  // @TODO
} ;




/**
 * ListBounds
 * List all points located into the parameters bounds
 *
 * @param  [array] bottomLeftPoint
 *         [array] topRightPoint
 *         function : cb : Callback function
 * @api private
 */

 LaunchPointCtrl.listPointsInBounds = function(req,res){
   // reverse because in mongodb coords are stored in long/lat
   var bottomLeftPoint = req.params.topleft.split(",").map(Number) ;
   var topRightPoint = req.params.bottomright.split(",").map(Number);

   const criteria = {
     geometry:{
       $geoWithin: { $box:  [ bottomLeftPoint.reverse(), topRightPoint.reverse() ] }
     }
   } ;
   LaunchPointModel.find(criteria,function(err,points){
     if (err) return res.send(err) ;
     else return  res.json(points) ;
   }) ;
 } ;

 LaunchPointCtrl.import = function(req,res){
   var imp = require('../import/document.json') ;
   var feats = imp.features;
   //res.json(feats[192]) ;
   //res.send(imp.features.length)  ;

   feats.forEach(function(item){
     var test = new LaunchPointModel(
       {
         geometry : {
           coordinates:[item.geometry.coordinates[0],item.geometry.coordinates[1],item.geometry.coordinates[2]]
         },
         properties : {
           name:item.properties.Name,
           body:item.properties.description
         }
       }
     );
     test.save(function (err, p) {
       if (err) return console.log(err);
       else return console.log("OK");
     });

   })
 } ;
module.exports = LaunchPointCtrl ;
