// Module dependencies
const mongoose =  require('mongoose');
const Schema =    mongoose.Schema;

const  bcrypt   = require('bcrypt-nodejs');


/*
 *
 *      User Schema
 *
 * * * * * * * * * * * * * * * * */
const User = new Schema({

  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  }

});

// Execute before each user.save() call
User.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  user.password = generateHash(user.password) ;
  callback() ;
});

/*
 *
 *      User Methods
 *
 * * * * * * * * * * * * * * * * */


User.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
User.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model and expose it to our app
module.exports = mongoose.model('User', User);
