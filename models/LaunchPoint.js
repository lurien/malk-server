// Module dependencies.
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Getters settters
const getTags = tags => tags.join(',');
const setTags = tags => tags.split(',');


/*
 *
 *      Launch Point Schema
 *
 * * * * * * * * * * * * * * * * */

const LaunchPoint = new Schema({
  type : {type: String, default : "Feature"},
  geometry : {
      type : {type : String, default : "Point"},
      coordinates: {type: [Number],index: '2dsphere' }
  },
  properties : {
    name: { type : String, default : '', required : true },
    body: { type : String, default : '' },
    user: { type : Schema.ObjectId, ref : 'User' },
    parking: { type : Boolean, default : false },
    difficulty: { type : Number },
    notes : { pos : Number, neg : Number },
    comments: [{
      body: { type : String, default : '' },
      user: { type : Schema.ObjectId, ref : 'User' },
      createdAt: { type : Date, default : Date.now }
    }],
    tags: { type: [], get: getTags, set: setTags },
    images: [{
      cdnUri: String,
      files: []
    }],
    createdAt  : { type : Date, default : Date.now }
  }
});

/*
 *
 *      Launch Point Validations
 *
 * * * * * * * * * * * * * * * * */

// Check blank name
LaunchPoint.path('properties.name').required(true, 'Article title cannot be blank');

// We check if a point already exist in 500 meters in the nearwood. If true we refuse the point.
LaunchPoint.path('geometry.coordinates').validate(function(value , callback){
  const criteria = {
    'geometry.coordinates':{
      $near : {
        $geometry: { type: "Point",  coordinates: value },
        $maxDistance: 200
      }
    }
  } ;
  console.log(criteria);
  const LaunchPoint = mongoose.model('LaunchPoint');
  LaunchPoint.find(criteria,function(err,points){
    if(err) throw err;
    callback(points.length == 0) ;
  }) ;
},'The point is too close from another.') ;



// create the model and expose it to our app
module.exports = mongoose.model('LaunchPoint', LaunchPoint);
