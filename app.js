var express       = require('express');
var passport      = require('passport');
var flash         = require('connect-flash');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var morgan        = require('morgan');
var session       = require('express-session');


var app = express();
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)

app.set('view engine', 'jade'); // set up ejs for templating
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// required for passport
app.use(session({ secret: 'youpiyoupitralala' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions


// routes requires
var routes = require('./routes/index');
var api = require('./routes/api')(passport);

// Database connection
var dbConfig = require('./config/database.js') ;
var dbController = require('./controllers/database.js') ;
dbController.connect(dbConfig.url) ;

// For all the routes
app.use(function(req,res,next){
    // Allow access from a different domain / port
    app.setHeaders(res) ;
    next();
});

// routes setup
app.use('/', routes);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.setHeaders = function(res){
  res.setHeader('Access-Control-Allow-Origin', '*');
  //res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, DELETE, UPDATE');
}

module.exports = app;
